# FXUUID #

FX_uuid reads the server's Product UUID value (from /sys on the running Linux OS) and writes that value to a register in
the GigaIO PCIe HBA card, which stores the value in NVM for later retrieval by the GigaIO FabreX switch (over the PCIe
cable via CMI).  This information is used to associate a specific server with the specific switch upstream port to which
it is connected. This function is intended to execute on every system boot (e.g., as part of systemd) to ensure that the
GigaIO PCIe HBA contains the Product UUID of the current server containing the HBA, in case the HBA might have been 
relocated to a different server since the last time the value was recorded in the HBA’s NVM.  On execution of fx_uuid, 
the GigaIO PCIe HBA compares the newly received Product UUID with the value already stored in its NVM and, if different, 
overwrites its NVM location with the updated value."

### Build ###

Verify Dependencies
```
$ apt-get install libpciaccess-dev
$ apt-get install uuid-dev
```
Compile
```
$ ./autogen.sh 
$ ./configure
$ make
```
Install
```
$ sudo make install
```
### Usage ###

```
$ sudo fx_uuid [OPTION]...

  -h : print help
  -u <uuid> : take the uuid from the command line (useful for testing). If -u missing, get it from /sys/class/dmi/id/product_uuid
  -w : write the uuid. If -w is missing, the utility reports the kuma cards with their serial number + the eventual uuid they contain.
  -V : print version and exit
```

### Copyright ###

fx_uuid is provided under the BSD license. The source code and accompanying documentation is copyright (c) 2021, GigaIO Networks. All rights reserved.
