#!/bin/sh
#
# (c) Copyright 2021 GigaIO Networks. All rights reserved.
#
# See LICENSE.txt for license information
#

libtoolize
aclocal
automake --gnu --add-missing
autoconf -i -f
