/*************************************************************************
 * (c) Copyright 2021 GigaIO Networks. All rights reserved.
 *
 * See LICENSE.txt for license information
 ************************************************************************/
#ifndef _FA404_H
#define _FA404_H

#include <stdint.h>
#include <pciaccess.h>
#include <uuid/uuid.h>

typedef struct fa404_access fa404_access_t;
struct fa404_access {
    int index;
    void *addr;
    struct pci_device *dev;
    uint64_t serial;
    fa404_access_t *next;
};

#define PEX88032            0xc010
#define AXI_ADDR(va)        *(volatile uint32_t *)((va) + 0x9f0100)
#define AXI_DATA(va)        *(volatile uint32_t *)((va) + 0x9f0104)
#define AXI_CMD(va)         *(volatile uint32_t *)((va) + 0x9f0108)
#define AXI_CMD_WRITE       0x1
#define AXI_CMD_READ        0x2
#define AXI_CMD_BUSY        0x4
#define AXI_CMD_READ_VALID  0x8
#define SHA_KEY_REG         0x2a020004
#define GPIO_IN_REG         0x2a080048
#define GPIO_OUT_REG        0x2a080058
#define GPIO_OUT_ATOMIC_REG 0x2a080060
#define GPIO_DV_NUM         17
#define GPIO_DV_MASK        (1 << GPIO_DV_NUM)
#define GPIO_ACK_NUM        18
#define GPIO_ACK_MASK       (1 << GPIO_ACK_NUM)
#define GPIO_DIR_REG        0x2a080000
#define GPIO_INPUT_EN_REG   0x2a080040
#define GPIO_ALT_REG        0xfff00804
#define FA404_OUT_ATOMIC(shift,mask,value)    (((shift)<<24)|((mask)<<8)|(value))
#define FA404_CMD_GUID_GET      0x04000000
#define FA404_CMD_GUID_GET0     0x04000000
#define FA404_CMD_GUID_GET1     0x04000001
#define FA404_CMD_GUID_GET2     0x04000002
#define FA404_CMD_GUID_SET      0x04100000
#define FA404_CMD_GUID_SET0     0x04100000
#define FA404_CMD_GUID_SET1     0x04100001
#define FA404_CMD_GUID_SET2     0x04100002

#endif
