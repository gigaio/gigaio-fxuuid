/*************************************************************************
 * (c) Copyright 2021 GigaIO Networks. All rights reserved.
 *
 * See LICENSE.txt for license information
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <pciaccess.h>
#include "fa404.h"

#define FX_VERSION "1.00"


// find capability
static int find_cap(struct pci_device *dev, uint8_t cap)
{
    uint16_t u16;
    uint8_t pos, id;
    int rc, ttl;

    rc = pci_device_cfg_read_u16(dev, &u16, 0x6);
    if (rc != 0) return (rc);
    if (!(u16 & 0x10)) return (-1);
    pos = 0x34;
    rc = pci_device_cfg_read_u8(dev, &pos, pos);
    ttl = 48;
    while (ttl--) {
        if (pos < 0x40) break;
        pos &= ~3;
        rc = pci_device_cfg_read_u16(dev, &u16, pos);
        id = u16 & 0xff;
        if (id == 0xff) break;
        if (id == cap) return (pos);
        pos = (u16 >> 8) & 0xff;
    }
    return (0);
}


static int find_ext_cap(struct pci_device *dev, int cap)
{
    uint32_t header;
    int ttl = 16;
    int pos = 0x100;
    int rc;

    rc = pci_device_cfg_read_u32(dev, &header, pos);
    if (rc != 0) return (rc);
    if (header == 0) return (-1);

    while (ttl-- > 0) {
        if ((header & 0xffff) == cap && pos != 0) {
            // found it
            return (pos);
        }
        pos = (header >> 20) & 0xffc;
        if (pos < 256) return (0);
        rc = pci_device_cfg_read_u32(dev, &header, pos);
        if (rc != 0) return (rc);
    }
    return (0);
}


// check if a device is usp
static int find_usp(struct pci_device *dev, int *is_usp)
{
    uint16_t u16;
    int pos, rc;

    pos = find_cap(dev, 0x10);
    if (pos) {
        rc = pci_device_cfg_read_u16(dev, &u16, pos+2);
        if (rc == 0) {
            *is_usp = ((u16 >> 4) & 0xf) == 0x5;
        }
        return (rc);
    }
    return (-1);
}


// find subvendor
static int find_subvendor(struct pci_device *dev, uint16_t *subvendor,
    uint16_t *subdevice)
{
    int pos, rc;
    uint32_t u32;

    pos = find_cap(dev, 0xd);
    if (pos) {
        rc = pci_device_cfg_read_u32(dev, &u32, pos+4);
        if (rc == 0) {
            *subvendor = u32 & 0xffff;
            *subdevice = u32 >> 16;
        }
    } else {
        rc = -1;
    }
    return (rc);
}


static int find_serial(struct pci_device *dev, uint64_t *serial)
{
    int pos, rc;
    uint32_t hi, lo;

    pos = find_ext_cap(dev, 0x3);
    if (pos) {
        rc = pci_device_cfg_read_u32(dev, &lo, pos+4);
        if (rc == 0) {
            rc = pci_device_cfg_read_u32(dev, &hi, pos+8);
            *serial = ((uint64_t)hi) << 32 | (uint64_t)lo;
        }
    } else {
        rc = -1;
    }

    return (rc);
}

// find all FA404 boards and return them in a linked list
static int find_fa404(fa404_access_t **hw)
{
    uid_t uid, euid;
    fa404_access_t *p, *old;
    int usp, rc, n;
    struct pci_device_iterator *iter;
    struct pci_id_match match;
    struct pci_device *dev;
    uint8_t type;
    uint16_t subvendor_id, subdevice_id;
    uint64_t serial;

    uid = getuid();
    euid = geteuid();
    if (uid != 0 && euid != 0) {
        printf("This program must be run as root\n");
        return (-1);
    }
    rc = pci_system_init();
    if (rc != 0) return (rc);

    n = 0;
    old = NULL;
    memset(&match, 0, sizeof (struct pci_id_match));
    match.vendor_id = 0x1000;
    match.device_id = PEX88032;
    match.subvendor_id = PCI_MATCH_ANY;
    match.subdevice_id = PCI_MATCH_ANY;
    iter = pci_id_match_iterator_create(&match);
    while ((dev = pci_device_next(iter)) != NULL) {
        rc = pci_device_probe(dev);
        if (rc == 0) {
            usp = 0;
            // get device type
            rc = pci_device_cfg_read_u8(dev, &type, 0xe);
            usp = 0;
            rc |= find_usp(dev, &usp);
            // filter: type must be 1, usp must be true
            if (rc == 0 && type == 1 && usp) {
                if (find_serial(dev, &serial) != 0) serial = 0ULL;
                if (find_subvendor(dev, &subvendor_id, &subdevice_id) != 0) {
                    subvendor_id = subdevice_id = 0;
                }
                if (subvendor_id == 0x1ce5 && subdevice_id == 0xff82) {
                    p = malloc(sizeof (fa404_access_t));
                    if (!p) return (-1);
                    p->index = n++;
                    p->dev = dev;
                    p->serial = serial;
                    p->addr = NULL;
                    p->next = NULL;
                    if (!old) {
                        *hw = p;
                    } else {
                        old->next = p;
                    }
                    old = p;
                }
            }
        }
    }
    return (0);
}


static int fa404_init(fa404_access_t *hw)
{
    return (pci_device_map_range(hw->dev, hw->dev->regions[0].base_addr,
            hw->dev->regions[0].size, PCI_DEV_MAP_FLAG_WRITABLE,
            (void **)&hw->addr));
}


void fa404_release(fa404_access_t *hw)
{
    pci_device_unmap_range(hw->dev, (void *)hw->addr, hw->dev->regions[0].size);
}


static int fa404_axi_read(void *va, uint32_t addr, uint32_t *val)
{
    AXI_ADDR(va) = addr;
    AXI_CMD(va) = AXI_CMD_READ;
    while (!(AXI_CMD(va) & AXI_CMD_READ_VALID));
    *val = AXI_DATA(va);

    return (0);
}


static int fa404_axi_write(void *va, uint32_t addr, uint32_t val, uint32_t mask)
{
    uint32_t u32;
    int rc;

    if (mask != 0 && mask != 0xffffffff) {
        rc = fa404_axi_read(va, addr, &u32);
        if (rc != 0) return (rc);
        val = (u32 & ~mask) | (val & mask);
    }
    AXI_ADDR(va) = addr;
    AXI_DATA(va) = val;
    AXI_CMD(va) = AXI_CMD_WRITE;
    while (AXI_CMD(va) & AXI_CMD_BUSY);

    return (0);
}


static int fa404_gpio_setup(void *va)
{
    uint32_t addr, data, mask;
    int pin;

    fa404_axi_read(va, GPIO_ALT_REG, &data);
    mask = (0x7 << 21) | (0x7 << 24);
    if (data & mask) {
        // the GPIOs are not configured properly. Redo the config!
        fprintf(stdout, "WARNING: reprogramming GPIO_17 and GPIO_18\n");
        // configure GPIO_17 and GPIO_18 as regular GPIOs
        fa404_axi_write(va, GPIO_ALT_REG, data & ~mask, 0);
        // GPIO_17 is output (push-pull)
        addr = GPIO_DIR_REG;
        pin = GPIO_DV_NUM;
        if (pin >= 16) {
            pin -= 16;
            addr += 4;
        }
        pin <<= 1;
        fa404_axi_write(va, addr, 0x3 << pin, 0x3 << pin);
        // GPIO_18 is input
        fa404_axi_write(va, GPIO_INPUT_EN_REG, GPIO_ACK_MASK, GPIO_ACK_MASK);
    }

    return (0);
}


static int fa404_comm_i(void *va, uint32_t *wbuf, int wlen, uint32_t *rbuf, int rlen)
{
    int i, retries;
    uint32_t u32;

    if (wlen > 8 || rlen > 8) return (-1);
    // pass data to PIC32
    for (i = 0; i < wlen; i++) {
        fa404_axi_write(va, SHA_KEY_REG+(i*4), wbuf[i], 0);
    }
    // tell PIC32 that data is ready
    fa404_axi_write(va, GPIO_OUT_ATOMIC_REG, FA404_OUT_ATOMIC(GPIO_DV_NUM, 1, 1), 0);
    // wait until PIC32 has done its job and ack'ed
    retries = 4000;
    do {
        usleep(250);
        fa404_axi_read(va, GPIO_IN_REG, &u32);
    } while (!(u32 & GPIO_ACK_MASK) && --retries);
    if (!retries) return (-1);
    // read result (if needed)
    for (i = 0; i < rlen; i++) {
        fa404_axi_read(va, SHA_KEY_REG+(i*4), &rbuf[i]);
    }
    // tell PIC32 we are done
    fa404_axi_write(va, GPIO_OUT_ATOMIC_REG, FA404_OUT_ATOMIC(GPIO_DV_NUM, 1, 0), 0);
    // wait until PIC32 also tell us it is done
    retries = 1000;
    do {
        fa404_axi_read(va, GPIO_IN_REG, &u32);
        if (!(u32 & GPIO_ACK_MASK)) return (0);
        --retries;
    } while (retries);

    return (-1);
}

static int fa404_comm(void *va, uint32_t *wbuf, int wlen, uint32_t *rbuf, int rlen)
{
    int rc, retry = 3;

    do {
        rc = fa404_comm_i(va, wbuf, wlen, rbuf, rlen);
    } while (rc != 0 && --retry > 0);

    return (rc);
}


static int fa404_uuid_get(void *base, uuid_t uuid)
{
    int rc;
    uint32_t wbuf[1], rbuf[5];

    wbuf[0] = FA404_CMD_GUID_GET1;
    rc = fa404_comm(base, wbuf, 1, rbuf, 5);
    if (rc != 0 || rbuf[0] != 0) return (-1);
    memcpy(uuid, &rbuf[1], sizeof (uuid_t));

    return (0);
}


static int fa404_uuid_set(void *base, uuid_t uuid)
{
    int rc;
    uint32_t wbuf[5], rbuf[1];

    wbuf[0] = FA404_CMD_GUID_SET1;
    memcpy(&wbuf[1], uuid, 4*sizeof(uint32_t));
    rc = fa404_comm(base, wbuf, 5, rbuf, 1);

    return ((rc == 0 && rbuf[0] == 0) ? 0 : -1);
}


static int get_product_uuid(uuid_t uuid)
{
    FILE *f;
    char s[512];

    f = fopen("/sys/class/dmi/id/product_uuid", "r");
    if (!f) return (-1);
    fgets(s, sizeof (s), f);
    s[strlen(s)-1] = '\0';
    fclose(f);

    return (uuid_parse(s, uuid));
}


static int program_uuid(fa404_access_t *hw, uuid_t uuid, int do_write)
{
    int rc;
    uuid_t u;
    char s[128];

    rc = fa404_init(hw);
    if (rc == 0) {
        fa404_gpio_setup(hw->addr);
        if (do_write) {
            rc = fa404_uuid_set(hw->addr, uuid);
            if (rc != 0) {
                fprintf(stderr, "Failed to program uuid on FA404 serial %016"
                    PRIx64"\n", hw->serial);
            }
        } else {
            rc = fa404_uuid_get(hw->addr, u);
            if (rc == 0) {
                uuid_unparse(u, s);
                printf("FA404 serial %016"PRIx64" : %s\n", hw->serial, s);
            } else {
                printf("FA404 serial %016"PRIx64" : not programmed\n",
                    hw->serial);
            }
        }
        fa404_release(hw);
    }
    return (rc);
}


static int program_all_pex(uuid_t uuid, int do_write)
{
    int rc;
    fa404_access_t *hw = NULL, *p;

    rc = find_fa404(&hw);
    if (rc == 0) {
        p = hw;
        while (p) {
            rc = program_uuid(p, uuid, do_write);
            // ignore rc (error alredy reported), keep going
            p = p->next;
        }
    }
    return (rc);
}


static void usage(const char *cmd, int rc)
{
    fprintf(stdout, "%s [-w [-u <uuid>]] [-V]\n", cmd);
    fprintf(stdout, "where\n");
    fprintf(stdout, "\t-w : copy host uuid to card(s)\n");
    fprintf(stdout, "\t-u <uuid> : if specified, use the given uuid instead of the system uuid\n");
    fprintf(stdout, "\t-V : print version and exit\n");
    exit(rc);
}


int main(int argc, char *argv[])
{
    int c, rc, do_write, do_version;
    uuid_t uuid;
    char uuid_str[128], *cmd = argv[0];

    strcpy(uuid_str, "\0");
    do_write = do_version = 0;

    while ((c = getopt(argc, argv, "hu:wV")) != EOF) {
        switch (c) {
        case 'h':
            usage(cmd, 0);
            break;
        case 'u':
            strncpy(uuid_str, optarg, sizeof (uuid_str));
            break;
        case 'w':
            do_write = 1;
            break;
        case 'V':
            do_version = 1;
            break;
        default:
            fprintf(stderr, "Invalid option\n");
            usage(cmd, -1);
        }
    }

    if (do_version) {
        fprintf(stdout, FX_VERSION"\n");
        return (0);
    }

    if (do_write) {
        if (uuid_str[0] == '\0') {
            rc = get_product_uuid(uuid);
            if (rc != 0) {
                fprintf(stderr, "Failed to get product_uuid from /sys\n");
                return (rc);
            }
        } else {
            if (uuid_parse(uuid_str, uuid) != 0) {
                fprintf(stderr, "Failed to parse '%s' as a uuid\n", uuid_str);
                return (-1);
            }
        }
    }

    rc = program_all_pex(uuid, do_write);
    if (do_write) {
        // run again in read mode
        program_all_pex(uuid, 0);
    }

    return (rc);
}
